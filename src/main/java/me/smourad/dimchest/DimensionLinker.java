package me.smourad.dimchest;

import me.smourad.dimchest.dto.DimChestDto;
import me.smourad.dimchest.dto.DimCode;
import me.smourad.dimchest.dto.DisplayType;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.ShulkerBox;
import org.bukkit.block.data.Directional;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.Transformation;
import org.jetbrains.annotations.Nullable;
import org.joml.AxisAngle4f;
import org.joml.Vector3f;

import java.util.*;

public class DimensionLinker implements Listener {

    private static final String DIMENSION_CODE_KEY = "DimChestDimensionCode";
    private static final String OWNER_KEY = "DimChestOwner";
    private static final String DISPLAY_KEY_UUID = "DimChestDisplayUUID";
    private static final String DISPLAY_KEY_MATERIAL = "DimChestDisplayMaterial";
    private static final String DISPLAY_KEY_TYPE = "DimChestDisplayType";

    private static final int ENDER_EYE_POSITION = 1;
    private static final int SHULKER_POSITION = 4;
    private static final int[] CODE_POSITION = new int[]{6, 7, 8};
    private static final int[] CHAIN_POSITION = new int[]{1, 3, 5, 7};
    private static final int[] EMPTY_LOCK_POSITION = new int[]{0, 2, 6, 8};
    private static final int[] EMPTY_LINK_POSITION = new int[]{0, 2, 3, 5};

    private final NamespacedKey dimensionCodeKey;
    private final NamespacedKey ownerKey;
    private final NamespacedKey displayUUIDKey;
    private final NamespacedKey displayMaterialKey;
    private final NamespacedKey displayTypeKey;

    public DimensionLinker() {
        dimensionCodeKey = new NamespacedKey(DimChestPlugin.INSTANCE, DIMENSION_CODE_KEY);
        ownerKey = new NamespacedKey(DimChestPlugin.INSTANCE, OWNER_KEY);
        displayUUIDKey = new NamespacedKey(DimChestPlugin.INSTANCE, DISPLAY_KEY_UUID);
        displayMaterialKey = new NamespacedKey(DimChestPlugin.INSTANCE, DISPLAY_KEY_MATERIAL);
        displayTypeKey = new NamespacedKey(DimChestPlugin.INSTANCE, DISPLAY_KEY_TYPE);
    }

    @EventHandler
    public void craft(PrepareItemCraftEvent event) {
        CraftingInventory inventory = event.getInventory();
        @Nullable ItemStack[] matrix = inventory.getMatrix();

        ItemStack eye = matrix[ENDER_EYE_POSITION];
        if (Objects.isNull(eye) || !Objects.equals(eye.getType(), Material.ENDER_EYE)) return;

        ItemStack shulker = matrix[SHULKER_POSITION];
        if (Objects.isNull(shulker) || !isShulker(shulker.getType())) return;

        if (shulker.getItemMeta() instanceof BlockStateMeta bsm) {
            ShulkerBox state = (ShulkerBox) bsm.getBlockState();
            if (!state.getInventory().isEmpty()) return;
        }

        for (int i : EMPTY_LINK_POSITION) {
            ItemStack part = matrix[i];
            if (Objects.nonNull(part)) return;
        }

        for (int i = 0; i < 3; i++) {
            ItemStack part = matrix[CODE_POSITION[i]];
            if (Objects.isNull(part)) return;
        }

        DimCode code = new DimCode(
                matrix[CODE_POSITION[0]].getType(),
                matrix[CODE_POSITION[1]].getType(),
                matrix[CODE_POSITION[2]].getType()
        );

        Material material = shulker.getType();
        ItemStack result = new ItemStack(material);
        ItemMeta meta = shulker.hasItemMeta()
                ? shulker.getItemMeta()
                : Bukkit.getItemFactory().getItemMeta(material);
        PersistentDataContainer persistentDataContainer = meta.getPersistentDataContainer();
        persistentDataContainer.set(dimensionCodeKey, PersistentDataType.STRING, DimChestPlugin.INSTANCE.getGson().toJson(code));
        if (!persistentDataContainer.has(ownerKey)) {
            persistentDataContainer.set(ownerKey, PersistentDataType.STRING, DimChestPlugin.INSTANCE.getGson().toJson(null));
        }

        setLore(meta);
        result.setItemMeta(meta);

        inventory.setResult(result);
    }

    @EventHandler
    public void lock(PrepareItemCraftEvent event) {
        CraftingInventory inventory = event.getInventory();
        @Nullable ItemStack[] matrix = inventory.getMatrix();

        for (int position : CHAIN_POSITION) {
            ItemStack chain = matrix[position];
            if (Objects.isNull(chain) || !Objects.equals(chain.getType(), Material.CHAIN)) return;
        }

        ItemStack shulker = matrix[SHULKER_POSITION];
        if (Objects.isNull(shulker) || !isShulker(shulker.getType())) return;

        for (int i : EMPTY_LOCK_POSITION) {
            ItemStack part = matrix[i];
            if (Objects.nonNull(part)) return;
        }

        if (shulker.hasItemMeta()) {
            ItemMeta meta = shulker.getItemMeta();
            UUID owner = DimChestPlugin.INSTANCE.getGson().fromJson(meta.getPersistentDataContainer().get(ownerKey, PersistentDataType.STRING), UUID.class);
            if (Objects.nonNull(owner)) return;
        }

        ItemMeta meta = shulker.getItemMeta();
        PersistentDataContainer persistentDataContainer = meta.getPersistentDataContainer();
        persistentDataContainer.set(ownerKey, PersistentDataType.STRING, DimChestPlugin.INSTANCE.getGson().toJson(event.getViewers().get(0).getUniqueId()));
        setLore(meta);
        shulker.setItemMeta(meta);

        inventory.setResult(shulker);
    }

    protected void setLore(ItemMeta meta) {
        List<Component> lore = new ArrayList<>();

        PersistentDataContainer persistentDataContainer = meta.getPersistentDataContainer();
        DimCode code = DimChestPlugin.INSTANCE.getGson().fromJson(
                persistentDataContainer.get(dimensionCodeKey, PersistentDataType.STRING),
                DimCode.class
        );

        lore.add(getElementOfCode(1, code.getFirst()));
        lore.add(getElementOfCode(2, code.getSecond()));
        lore.add(getElementOfCode(3, code.getThird()));

        UUID owner = DimChestPlugin.INSTANCE.getGson().fromJson(persistentDataContainer.get(ownerKey, PersistentDataType.STRING), UUID.class);
        if (Objects.nonNull(owner)) {
            Component header = Component.text("➡ ").color(NamedTextColor.GRAY);
            Component name = Component.text(Bukkit.getOfflinePlayer(owner).getName()).color(NamedTextColor.GRAY);
            lore.add(header.append(name));
        }

        meta.lore(lore.stream()
                .map(line -> line.decoration(TextDecoration.ITALIC, false))
                .toList()
        );
    }

    protected Component getElementOfCode(int i, Material material) {
        Component number = Component.text(i, NamedTextColor.WHITE);
        Component separator = Component.text(": ").color(NamedTextColor.WHITE);
        Component element = Component.translatable(material.translationKey()).color(NamedTextColor.WHITE);

        return number.append(separator).append(element);
    }

    @EventHandler
    public void place(BlockPlaceEvent event) {
        ItemStack item = event.getItemInHand();
        if (!isShulker(item.getType())) return;

        Block block = event.getBlockPlaced();
        if (!item.hasItemMeta()) return;

        ItemMeta meta = item.getItemMeta();
        PersistentDataContainer itemPersistentDataContainer = meta.getPersistentDataContainer();
        DimCode code = DimChestPlugin.INSTANCE.getGson().fromJson(itemPersistentDataContainer.get(dimensionCodeKey, PersistentDataType.STRING), DimCode.class);
        if (Objects.isNull(code)) return;

        UUID owner = DimChestPlugin.INSTANCE.getGson().fromJson(itemPersistentDataContainer.get(ownerKey, PersistentDataType.STRING), UUID.class);

        ShulkerBox box = (ShulkerBox) block.getState();
        new BukkitRunnable() {
            @Override
            public void run() {
                box.getInventory().clear();
                box.getInventory().addItem(new ItemStack(Material.EMERALD));
            }
        }.runTaskLater(DimChestPlugin.INSTANCE, 0);
        PersistentDataContainer blockPersistentDataContainer = box.getPersistentDataContainer();
        blockPersistentDataContainer.set(dimensionCodeKey, PersistentDataType.STRING, DimChestPlugin.INSTANCE.getGson().toJson(code));
        blockPersistentDataContainer.set(ownerKey, PersistentDataType.STRING, DimChestPlugin.INSTANCE.getGson().toJson(owner));
        box.update();

        DimChestPlugin.INSTANCE.getDimensionKeeper().place(owner, code, block.getLocation());
    }

    @EventHandler
    public void onItemMove(InventoryMoveItemEvent event) {
        Inventory source = event.getSource();
        Inventory destination = event.getDestination();

        give(event, source, destination);
        put(event, destination);
    }

    private void give(InventoryMoveItemEvent event, Inventory source, Inventory destination) {
        InventoryType sourceType = source.getType();
        if (sourceType.equals(InventoryType.SHULKER_BOX) && source.getHolder() instanceof ShulkerBox box) {
            PersistentDataContainer blockPersistentDataContainer = box.getPersistentDataContainer();
            DimCode code = DimChestPlugin.INSTANCE.getGson().fromJson(blockPersistentDataContainer.get(dimensionCodeKey, PersistentDataType.STRING), DimCode.class);
            if (Objects.nonNull(code)) {
                UUID owner = DimChestPlugin.INSTANCE.getGson().fromJson(blockPersistentDataContainer.get(ownerKey, PersistentDataType.STRING), UUID.class);

                Inventory inventory = DimChestPlugin.INSTANCE.getDimensionKeeper().getInventory(owner, code);
                extract(destination, inventory);

                event.setCancelled(true);
            }
        }
    }

    protected void extract(Inventory destination, Inventory inventory) {
        for (int i = 0; i < inventory.getSize(); i++) {
            ItemStack item = inventory.getItem(i);
            if (Objects.nonNull(item)) {
                ItemStack result = item.clone();
                result.setAmount(1);

                Map<Integer, ItemStack> rest = destination.addItem(result);
                if (rest.isEmpty()) {
                    item.setAmount(item.getAmount() - 1);
                    inventory.setItem(i, item);
                    break;
                }
            }
        }
    }

    protected void put(InventoryMoveItemEvent event, Inventory destination) {
        InventoryType destinationType = destination.getType();
        if (destinationType.equals(InventoryType.SHULKER_BOX) && destination.getHolder() instanceof ShulkerBox box) {
            PersistentDataContainer blockPersistentDataContainer = box.getPersistentDataContainer();
            DimCode code = DimChestPlugin.INSTANCE.getGson().fromJson(blockPersistentDataContainer.get(dimensionCodeKey, PersistentDataType.STRING), DimCode.class);
            if (Objects.nonNull(code)) {
                UUID owner = DimChestPlugin.INSTANCE.getGson().fromJson(blockPersistentDataContainer.get(ownerKey, PersistentDataType.STRING), UUID.class);

                Inventory inventory = DimChestPlugin.INSTANCE.getDimensionKeeper().getInventory(owner, code);

                ItemStack item = event.getItem();
                if (inventory.addItem(item).isEmpty()) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            destination.removeItem(item);
                        }
                    }.runTaskLater(DimChestPlugin.INSTANCE, 0);
                } else {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onOpen(InventoryOpenEvent event) {
        Inventory inventory = event.getInventory();
        InventoryType type = inventory.getType();

        if (type.equals(InventoryType.SHULKER_BOX) && inventory.getHolder() instanceof ShulkerBox box) {
            PersistentDataContainer blockPersistentDataContainer = box.getPersistentDataContainer();
            DimCode code = DimChestPlugin.INSTANCE.getGson().fromJson(blockPersistentDataContainer.get(dimensionCodeKey, PersistentDataType.STRING), DimCode.class);
            if (Objects.isNull(code)) return;

            UUID owner = DimChestPlugin.INSTANCE.getGson().fromJson(blockPersistentDataContainer.get(ownerKey, PersistentDataType.STRING), UUID.class);

            HumanEntity player = event.getPlayer();
            DimChestPlugin.INSTANCE.getDimensionKeeper().open(player, owner, code);
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        Inventory inventory = event.getInventory();
        DimChestPlugin.INSTANCE.getDimensionKeeper().close(event.getPlayer(), inventory);
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        Material material = block.getType();
        if (!isShulker(material)) return;

        ShulkerBox box = (ShulkerBox) block.getState();
        PersistentDataContainer blockPersistentDataContainer = box.getPersistentDataContainer();
        DimCode code = DimChestPlugin.INSTANCE.getGson().fromJson(blockPersistentDataContainer.get(dimensionCodeKey, PersistentDataType.STRING), DimCode.class);
        if (Objects.isNull(code)) return;

        UUID owner = DimChestPlugin.INSTANCE.getGson().fromJson(blockPersistentDataContainer.get(ownerKey, PersistentDataType.STRING), UUID.class);

        DimChestDto chest = DimChestPlugin.INSTANCE.getDimensionKeeper().getChest(owner, code, block.getLocation());
        DimChestPlugin.INSTANCE.getDimensionKeeper().remove(owner, code, block.getLocation());

        ItemStack result = new ItemStack(material);
        ItemMeta meta = Bukkit.getItemFactory().getItemMeta(material);
        PersistentDataContainer persistentDataContainer = meta.getPersistentDataContainer();
        persistentDataContainer.set(dimensionCodeKey, PersistentDataType.STRING, DimChestPlugin.INSTANCE.getGson().toJson(code));
        persistentDataContainer.set(ownerKey, PersistentDataType.STRING, DimChestPlugin.INSTANCE.getGson().toJson(owner));
        setLore(meta);
        result.setItemMeta(meta);

        event.setDropItems(false);
        World world = block.getWorld();
        world.dropItemNaturally(block.getLocation(), result);

        block.getWorld()
                .getNearbyEntitiesByType(ItemDisplay.class, block.getLocation(), 2)
                .stream()
                .filter(id -> {
                    PersistentDataContainer pdc = id.getPersistentDataContainer();
                    String data = pdc.get(displayUUIDKey, PersistentDataType.STRING);
                    return Objects.nonNull(data) && Objects.equals(UUID.fromString(data), chest.getUuid());
                })
                .forEach(Entity::remove);

        block.getWorld()
                .getNearbyEntitiesByType(TextDisplay.class, block.getLocation(), 2)
                .stream()
                .filter(id -> {
                    PersistentDataContainer pdc = id.getPersistentDataContainer();
                    String data = pdc.get(displayUUIDKey, PersistentDataType.STRING);
                    return Objects.nonNull(data) && Objects.equals(UUID.fromString(data), chest.getUuid());
                })
                .forEach(Entity::remove);
    }

    protected boolean isShulker(Material material) {
        return switch (material) {
            case SHULKER_BOX, BLUE_SHULKER_BOX, BLACK_SHULKER_BOX,
                    CYAN_SHULKER_BOX, GRAY_SHULKER_BOX, BROWN_SHULKER_BOX,
                    GREEN_SHULKER_BOX, LIME_SHULKER_BOX, ORANGE_SHULKER_BOX,
                    PINK_SHULKER_BOX, PURPLE_SHULKER_BOX, RED_SHULKER_BOX,
                    WHITE_SHULKER_BOX, YELLOW_SHULKER_BOX, MAGENTA_SHULKER_BOX,
                    LIGHT_BLUE_SHULKER_BOX, LIGHT_GRAY_SHULKER_BOX -> true;
            default -> false;
        };
    }

    @EventHandler
    public void render(BlockPhysicsEvent event) {
        Block block = event.getSourceBlock();
        Material material = block.getType();
        if (!isShulker(material)) return;

        ShulkerBox box = (ShulkerBox) block.getState();
        PersistentDataContainer blockPersistentDataContainer = box.getPersistentDataContainer();
        DimCode code = DimChestPlugin.INSTANCE.getGson().fromJson(blockPersistentDataContainer.get(dimensionCodeKey, PersistentDataType.STRING), DimCode.class);
        if (Objects.isNull(code)) return;

        UUID owner = DimChestPlugin.INSTANCE.getGson().fromJson(blockPersistentDataContainer.get(ownerKey, PersistentDataType.STRING), UUID.class);

        DimChestDto chest = DimChestPlugin.INSTANCE.getDimensionKeeper().getChest(owner, code, block.getLocation());
        UUID uuid = chest.getUuid();

        List<ItemDisplay> items = new ArrayList<>(block.getWorld()
                .getNearbyEntitiesByType(ItemDisplay.class, block.getLocation(), 2)
                .stream()
                .filter(id -> {
                    PersistentDataContainer persistentDataContainer = id.getPersistentDataContainer();
                    String data = persistentDataContainer.get(displayUUIDKey, PersistentDataType.STRING);
                    return Objects.nonNull(data) && Objects.equals(UUID.fromString(data), uuid);
                })
                .toList());

        renderCode(block, items, code, uuid);
        items.forEach(Entity::remove);

        List<TextDisplay> texts = new ArrayList<>(block.getWorld()
                .getNearbyEntitiesByType(TextDisplay.class, block.getLocation(), 2)
                .stream()
                .filter(id -> {
                    PersistentDataContainer persistentDataContainer = id.getPersistentDataContainer();
                    String data = persistentDataContainer.get(displayUUIDKey, PersistentDataType.STRING);
                    return Objects.nonNull(data) && Objects.equals(UUID.fromString(data), uuid);
                })
                .toList());

        if (Objects.nonNull(owner)) {
            renderLock(block, texts, owner, uuid);
        }

        texts.forEach(Entity::remove);
    }

    protected void renderLock(Block block, List<TextDisplay> display, UUID owner, UUID uuid) {
        Directional directional = (Directional) block.getBlockData();
        BlockFace facing = directional.getFacing();
        BoundingBox bb = block.getBoundingBox();
        World world = block.getWorld();

        Transformation transformation = new Transformation(
                new Vector3f(0, 0, 0),
                new AxisAngle4f(0, 0, 0, 1),
                new Vector3f(0.5f, 0.5f, 0.5f),
                new AxisAngle4f(0, 0, 0, 1)
        );

        TextDisplay text;
        if (display.isEmpty()) {
            text = (TextDisplay) world.spawnEntity(block.getLocation(), EntityType.TEXT_DISPLAY);
            text.setBillboard(Display.Billboard.FIXED);
            text.setTransformation(transformation);

            PersistentDataContainer persistentDataContainer = text.getPersistentDataContainer();
            persistentDataContainer.set(displayUUIDKey, PersistentDataType.STRING, uuid.toString());
            persistentDataContainer.set(displayTypeKey, PersistentDataType.STRING, DisplayType.LOCK.name());
        } else {
            text = display.get(0);
            display.remove(text);
        }

        text.setText(Bukkit.getOfflinePlayer(owner).getName());

        switch (facing) {
            case UP -> {
                double x = bb.getCenterX();
                double z = bb.getCenterZ();
                double y = bb.getMaxY();

                text.teleport(new Location(world, x, y, z + 0.15, 180f, -90f));
            }
            case DOWN -> {
                double x = bb.getCenterX();
                double z = bb.getCenterZ();
                double y = bb.getMinY();

                text.teleport(new Location(world, x, y, z + 0.15, 180f, 90f));
            }
            case WEST -> {
                double x = bb.getMinX();
                double z = bb.getCenterZ();
                double y = bb.getCenterY();

                text.teleport(new Location(world, x, y + 0.15, z, 90f, 0f));
            }
            case EAST -> {
                double x = bb.getMaxX();
                double z = bb.getCenterZ();
                double y = bb.getCenterY();

                text.teleport(new Location(world, x, y + 0.15, z, -90f, 0f));
            }
            case SOUTH -> {
                double x = bb.getCenterX();
                double z = bb.getMaxZ();
                double y = bb.getCenterY();

                text.teleport(new Location(world, x, y + 0.15, z, 0f, 0f));
            }
            case NORTH -> {
                double x = bb.getCenterX();
                double z = bb.getMinZ();
                double y = bb.getCenterY();

                text.teleport(new Location(world, x, y + 0.15, z, 180f, 0f));
            }
        }
    }

    protected void renderCode(Block block, List<ItemDisplay> display, DimCode code, UUID uuid) {
        Directional directional = (Directional) block.getBlockData();
        BlockFace facing = directional.getFacing();
        BoundingBox bb = block.getBoundingBox();
        World world = block.getWorld();
        Transformation transformation = new Transformation(
                new Vector3f(0, 0, 0),
                new AxisAngle4f(0, 0, 0, 1),
                new Vector3f(0.2f, 0.2f, 0.2f),
                new AxisAngle4f(0, 0, 0, 1)
        );

        ItemDisplay first = getFromDisplay(display, code.getFirst());
        if (Objects.isNull(first)) {
            first = (ItemDisplay) world.spawnEntity(block.getLocation(), EntityType.ITEM_DISPLAY);
            first.setItemStack(new ItemStack(code.getFirst()));
            first.setTransformation(transformation);
            first.setBillboard(Display.Billboard.FIXED);

            PersistentDataContainer persistentDataContainer = first.getPersistentDataContainer();
            persistentDataContainer.set(displayMaterialKey, PersistentDataType.STRING, code.getFirst().name());
            persistentDataContainer.set(displayUUIDKey, PersistentDataType.STRING, uuid.toString());
            persistentDataContainer.set(displayTypeKey, PersistentDataType.STRING, DisplayType.CODE.name());
        } else {
            display.remove(first);
        }

        ItemDisplay second = getFromDisplay(display, code.getSecond());
        if (Objects.isNull(second)) {
            second = (ItemDisplay) world.spawnEntity(block.getLocation(), EntityType.ITEM_DISPLAY);
            second.setItemStack(new ItemStack(code.getSecond()));
            second.setTransformation(transformation);
            second.setBillboard(Display.Billboard.FIXED);

            PersistentDataContainer persistentDataContainer = second.getPersistentDataContainer();
            persistentDataContainer.set(displayMaterialKey, PersistentDataType.STRING, code.getSecond().name());
            persistentDataContainer.set(displayUUIDKey, PersistentDataType.STRING, uuid.toString());
            persistentDataContainer.set(displayTypeKey, PersistentDataType.STRING, DisplayType.CODE.name());
        } else {
            display.remove(second);
        }

        ItemDisplay third = getFromDisplay(display, code.getThird());
        if (Objects.isNull(third)) {
            third = (ItemDisplay) world.spawnEntity(block.getLocation(), EntityType.ITEM_DISPLAY);
            third.setItemStack(new ItemStack(code.getThird()));
            third.setTransformation(transformation);
            third.setBillboard(Display.Billboard.FIXED);

            PersistentDataContainer persistentDataContainer = third.getPersistentDataContainer();
            persistentDataContainer.set(displayMaterialKey, PersistentDataType.STRING, code.getThird().name());
            persistentDataContainer.set(displayUUIDKey, PersistentDataType.STRING, uuid.toString());
            persistentDataContainer.set(displayTypeKey, PersistentDataType.STRING, DisplayType.CODE.name());
        } else {
            display.remove(third);
        }

        switch (facing) {
            case UP -> {
                double x = bb.getCenterX();
                double z = bb.getCenterZ();
                double y = bb.getMaxY();

                first.teleport(new Location(world, x + 0.25, y, z, 0f, 90f));
                second.teleport(new Location(world, x, y, z, 0f, 90f));
                third.teleport(new Location(world, x - 0.25, y, z, 0f, 90f));
            }
            case DOWN -> {
                double x = bb.getCenterX();
                double z = bb.getCenterZ();
                double y = bb.getMinY();

                first.teleport(new Location(world, x - 0.25, y, z, 0f, -90f));
                second.teleport(new Location(world, x, y, z, 0f, -90f));
                third.teleport(new Location(world, x + 0.25, y, z, 0f, -90f));
            }
            case EAST -> {
                double x = bb.getMaxX();
                double z = bb.getCenterZ();
                double y = bb.getCenterY();

                first.teleport(new Location(world, x, y, z + 0.25, 90f, 0f));
                second.teleport(new Location(world, x, y, z, 90f, 0f));
                third.teleport(new Location(world, x, y, z - 0.25, 90f, 0f));
            }
            case WEST -> {
                double x = bb.getMinX();
                double z = bb.getCenterZ();
                double y = bb.getCenterY();

                first.teleport(new Location(world, x, y, z - 0.25, -90f, 0f));
                second.teleport(new Location(world, x, y, z, -90f, 0f));
                third.teleport(new Location(world, x, y, z + 0.25, -90f, 0f));
            }
            case NORTH -> {
                double x = bb.getCenterX();
                double z = bb.getMinZ();
                double y = bb.getCenterY();

                first.teleport(new Location(world, x + 0.25, y, z, 0f, 0f));
                second.teleport(new Location(world, x, y, z, 0f, 0f));
                third.teleport(new Location(world, x - 0.25, y, z, 0f, 0f));
            }
            case SOUTH -> {
                double x = bb.getCenterX();
                double z = bb.getMaxZ();
                double y = bb.getCenterY();

                first.teleport(new Location(world, x + 0.25, y, z, 180f, 0f));
                second.teleport(new Location(world, x, y, z, 180f, 0f));
                third.teleport(new Location(world, x - 0.25, y, z, 180f, 0f));
            }
        }
    }

    protected @Nullable ItemDisplay getFromDisplay(List<ItemDisplay> display, Material material) {
        return display.stream()
                .filter(id -> {
                    PersistentDataContainer persistentDataContainer = id.getPersistentDataContainer();
                    String data = persistentDataContainer.get(displayMaterialKey, PersistentDataType.STRING);
                    return Objects.nonNull(data) && Objects.equals(Material.valueOf(data), material);
                })
                .findFirst()
                .orElse(null);
    }

}
