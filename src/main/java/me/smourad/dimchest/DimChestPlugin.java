package me.smourad.dimchest;

import com.google.gson.Gson;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class DimChestPlugin extends JavaPlugin {

    public static DimChestPlugin INSTANCE;

    private DimensionKeeper dimensionKeeper;
    private Gson gson;

    @Override
    public void onEnable() {
        INSTANCE = this;
        gson = new Gson();

        dimensionKeeper = new DimensionKeeper();
        DimensionLinker dimensionLinker = new DimensionLinker();
        getServer().getPluginManager().registerEvents(dimensionLinker, this);
    }

    @Override
    public void onDisable() {
        dimensionKeeper.save();
        dimensionKeeper.unloadChunks();
    }

}
