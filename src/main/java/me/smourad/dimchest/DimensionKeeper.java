package me.smourad.dimchest;

import com.google.common.io.Files;
import me.smourad.dimchest.data.DimChest;
import me.smourad.dimchest.data.DimInventory;
import me.smourad.dimchest.dto.DimChestDto;
import me.smourad.dimchest.dto.DimCode;
import me.smourad.dimchest.dto.DimIdentifierDto;
import me.smourad.dimchest.dto.DimInventoryDto;
import me.smourad.dimchest.utils.FileUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.ShulkerBox;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.BoundingBox;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DimensionKeeper {

    private static final String PRIVATE_FOLDER_NAME = "private";
    private static final String DIM_CHEST_DATA_FILE_NAME = "%s-%s-%s";
    private static final String DIM_CHEST_DATA_REGEX = "(.*)-(.*)-(.*)";
    private static final String PUBLIC_FOLDER_NAME = "public";

    private final Map<UUID, Map<DimCode, DimInventoryDto>> privateChests;
    private final Map<DimCode, DimInventoryDto> publicChests;
    private final Map<Inventory, DimIdentifierDto> openChests = new HashMap<>();
    private final Map<World, List<BoundingBox>> areaPerWorld = new HashMap<>();

    private final Pattern dimChestDataPattern;

    public DimensionKeeper() {
        dimChestDataPattern = Pattern.compile(DIM_CHEST_DATA_REGEX);

        privateChests = loadPrivateChests();
        publicChests = loadPublicChests();

        loadChunks();

        new BukkitRunnable() {
            @Override
            public void run() {
                save();
            }
        }.runTaskTimerAsynchronously(DimChestPlugin.INSTANCE, 60 * 20L, 60 * 20L);
    }

    protected void loadChunks() {
        int viewDistance = Bukkit.getViewDistance() * 16;
        Map<World, List<BoundingBox>> result = Stream
                .concat(privateChests.values().stream()
                                .flatMap(data -> data.values().stream()
                                        .flatMap(inv -> inv.getChests().stream())
                                ),
                        publicChests.values().stream()
                                .flatMap(inv -> inv.getChests().stream())
                )
                .collect(Collectors.groupingBy(
                        chest -> chest.getLocation().getWorld(),
                        Collectors.mapping(chest -> {
                            Location anchor = chest.getLocation().clone();
                            anchor.setY(0);
                            return BoundingBox.of(anchor, viewDistance, viewDistance, viewDistance);
                        }, Collectors.toList()))
                );

        for (Map.Entry<World, List<BoundingBox>> entry : result.entrySet()) {
            World world = entry.getKey();

            for (BoundingBox bb : entry.getValue()) {
                loadChunks(bb, world);
            }
        }
    }

    protected void loadChunks(BoundingBox bb, World world) {
        int chunkX1 = (int) (bb.getMinX() / 16);
        int chunkZ1 = (int) (bb.getMinZ() / 16);
        int chunkX2 = (int) (bb.getMaxX() / 16);
        int chunkZ2 = (int) (bb.getMaxZ() / 16);

        for (int x = chunkX1; x <= chunkX2; x++) {
            for (int z = chunkZ1; z <= chunkZ2; z++) {
                Chunk chunk = world.getChunkAt(x, z);
                chunk.load();
                chunk.setForceLoaded(true);
            }
        }

        areaPerWorld.putIfAbsent(world, new ArrayList<>());
        areaPerWorld.get(world).add(bb);
    }

    public void unloadChunks() {
        for (Map.Entry<World, List<BoundingBox>> entry : areaPerWorld.entrySet()) {
            World world = entry.getKey();

            for (BoundingBox bb : entry.getValue().stream().toList()) {
                unloadChunks(bb, world);
            }
        }
    }

    public void unloadChunks(BoundingBox bb, World world) {
        int chunkX1 = (int) (bb.getMinX() / 16);
        int chunkZ1 = (int) (bb.getMinZ() / 16);
        int chunkX2 = (int) (bb.getMaxX() / 16);
        int chunkZ2 = (int) (bb.getMaxZ() / 16);

        for (int x = chunkX1; x <= chunkX2; x++) {
            for (int z = chunkZ1; z <= chunkZ2; z++) {
                Chunk chunk = world.getChunkAt(x, z);
                chunk.unload();
                chunk.setForceLoaded(false);
            }
        }

        areaPerWorld.get(world).removeIf(bb::equals);
    }

    protected Map<DimCode, DimInventoryDto> loadPublicChests() {
        Path path = DimChestPlugin.INSTANCE.getDataFolder().toPath().resolve(PUBLIC_FOLDER_NAME);
        if (FileUtils.exist(path)) {
            return getDimChestsFromPath(path);
        } else {
            return new HashMap<>();
        }
    }

    protected Map<UUID, Map<DimCode, DimInventoryDto>> loadPrivateChests() {
        Path path = DimChestPlugin.INSTANCE.getDataFolder().toPath().resolve(PRIVATE_FOLDER_NAME);
        if (FileUtils.exist(path)) {
            return FileUtils.getAllFolders(path)
                    .stream()
                    .collect(Collectors.toMap(
                            folder -> {
                                String filename = folder.getFileName().toString();
                                return UUID.fromString(filename);
                            },
                            this::getDimChestsFromPath
                    ));
        } else {
            return new HashMap<>();
        }
    }

    protected Map<DimCode, DimInventoryDto> getDimChestsFromPath(Path path) {
        return FileUtils.readAll(path)
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        entry -> {
                            String filename = Files.getNameWithoutExtension(entry.getKey().getFileName().toString());
                            Matcher matcher = dimChestDataPattern.matcher(filename);
                            if (matcher.find()) {
                                return new DimCode(
                                        Material.valueOf(matcher.group(1)),
                                        Material.valueOf(matcher.group(2)),
                                        Material.valueOf(matcher.group(3))
                                );
                            } else {
                                throw new RuntimeException("");
                            }
                        },
                        entry -> {
                            DimInventory data = DimChestPlugin.INSTANCE.getGson().fromJson(entry.getValue(), DimInventory.class);
                            Inventory inventory = Bukkit.createInventory(null, InventoryType.SHULKER_BOX);

                            Map<String, Object>[] items = data.getItems();
                            for (int i = 0; i < items.length; i++) {
                                if (Objects.nonNull(items[i])) {
                                    inventory.setItem(i, ItemStack.deserialize(items[i]));
                                }
                            }

                            List<DimChestDto> chests = data.getChests().stream()
                                    .map(chest -> new DimChestDto()
                                            .setLocation(Location.deserialize(chest.getLocation()))
                                            .setUuid(chest.getUuid())
                                    )
                                    .toList();

                            return new DimInventoryDto()
                                    .setInventory(inventory)
                                    .setChests(new ArrayList<>(chests));
                        }
                ));
    }

    public Inventory getInventory(@Nullable UUID owner, DimCode code) {
        DimInventoryDto inventory = Objects.isNull(owner)
                ? publicChests.get(code)
                : privateChests.get(owner).get(code);

        return inventory.getInventory();
    }

    public void open(HumanEntity player, @Nullable UUID owner, DimCode code) {
        DimInventoryDto inventory = Objects.isNull(owner)
                ? publicChests.get(code)
                : privateChests.get(owner).get(code);

        Inventory inv = inventory.getInventory();
        player.openInventory(inv);
        openChests.put(inv, new DimIdentifierDto(code, owner));

        List<DimChestDto> buffer = new ArrayList<>();

        inventory.getChests()
                .forEach(chest -> {
                    Block block = chest.getLocation().getWorld().getBlockAt(chest.getLocation());
                    if (block.getState() instanceof ShulkerBox box) {
                        box.open();
                    } else {
                       buffer.add(chest);
                    }
                });

        buffer.forEach(chest -> remove(owner, code, chest.getLocation()));
    }

    public void close(HumanEntity player, Inventory inv) {
        if (!openChests.containsKey(inv)) return;

        DimIdentifierDto identifier = openChests.get(inv);
        UUID owner = identifier.owner();
        DimCode code = identifier.code();

        DimInventoryDto inventory = Objects.isNull(owner)
                ? publicChests.get(code)
                : privateChests.get(owner).get(code);

        new BukkitRunnable() {
            @Override
            public void run() {
                inv.getViewers().remove(player);
                if (inv.getViewers().isEmpty()) {
                    inventory.getChests().forEach(chest -> {
                        Block block = chest.getLocation().getWorld().getBlockAt(chest.getLocation());
                        ShulkerBox box = (ShulkerBox) block.getState();
                        box.close();
                    });

                    openChests.remove(inv);
                }
            }
        }.runTaskLater(DimChestPlugin.INSTANCE, 0);
    }

    public DimChestDto place(@Nullable UUID owner, DimCode code, Location location) {
        DimChestDto chest = new DimChestDto()
                .setLocation(location)
                .setUuid(UUID.randomUUID());

        if (Objects.isNull(owner)) {
            publicChests.computeIfAbsent(code, c -> new DimInventoryDto()
                    .setInventory(Bukkit.createInventory(null, InventoryType.SHULKER_BOX))
                    .setChests(new ArrayList<>())
            );

            publicChests.get(code).getChests().add(chest);
        } else {
            privateChests.putIfAbsent(owner, new HashMap<>());
            Map<DimCode, DimInventoryDto> inventories = privateChests.get(owner);

            inventories.computeIfAbsent(code, c -> new DimInventoryDto()
                    .setInventory(Bukkit.createInventory(null, InventoryType.SHULKER_BOX))
                    .setChests(new ArrayList<>())
            );

            inventories.get(code).getChests().add(chest);
        }

        int viewDistance = Bukkit.getViewDistance() * 16;
        Location anchor = location.clone();
        anchor.setY(0);
        BoundingBox bb = BoundingBox.of(anchor, viewDistance, viewDistance, viewDistance);
        loadChunks(bb, anchor.getWorld());

        return chest;
    }

    public void remove(@Nullable UUID owner, DimCode code, Location location) {
        if (Objects.isNull(owner)) {
            remove(code, location, publicChests);
        } else {
            Map<DimCode, DimInventoryDto> inventories = privateChests.get(owner);
            remove(code, location, inventories);
        }
    }

    private void remove(DimCode code, Location location, Map<DimCode, DimInventoryDto> inventories) {
        int viewDistance = Bukkit.getViewDistance() * 16;
        inventories.get(code).getChests()
                .removeIf(chest -> {
                    if (Objects.equals(chest.getLocation(), location)) {
                        Location anchor = location.clone();
                        anchor.setY(0);
                        BoundingBox bb = BoundingBox.of(anchor, viewDistance, viewDistance, viewDistance);
                        unloadChunks(bb, location.getWorld());
                        return true;
                    }

                    return false;
                });
    }

    public synchronized void save() {
        Path origin = DimChestPlugin.INSTANCE.getDataFolder().toPath();
        Path publicFolder = origin.resolve(PUBLIC_FOLDER_NAME);
        save(publicFolder, publicChests);

        Path privateFolder = origin.resolve(PRIVATE_FOLDER_NAME);
        for (Map.Entry<UUID, Map<DimCode, DimInventoryDto>> entry : privateChests.entrySet()) {
            Path folder = privateFolder.resolve(entry.getKey().toString());
            save(folder, entry.getValue());
        }
    }

    protected void save(Path dir, Map<DimCode, DimInventoryDto> data) {
        Map<String, DimInventory> formatted = data.entrySet().stream()
                .collect(Collectors.toMap(
                        entry -> {
                            DimCode code = entry.getKey();
                            return String.format(DIM_CHEST_DATA_FILE_NAME, code.getFirst(), code.getSecond(), code.getThird());
                        },
                        entry -> {
                            DimInventoryDto dto = entry.getValue();

                            List<DimChest> chests = dto.getChests().stream()
                                    .map(chest -> new DimChest()
                                            .setLocation(chest.getLocation().serialize())
                                            .setUuid(chest.getUuid())
                                    ).toList();

                            Inventory inventory = dto.getInventory();
                            int size = inventory.getSize();
                            Map<String, Object>[] items = new Map[size];
                            for (int i = 0; i < size; i++) {
                                ItemStack item = inventory.getItem(i);
                                if (Objects.nonNull(item)) {
                                    items[i] = item.serialize();
                                }
                            }

                            return new DimInventory()
                                    .setChests(chests)
                                    .setItems(items);
                        }
                ));

        formatted.forEach((filename, inventory) ->
                FileUtils.write(dir, filename, DimChestPlugin.INSTANCE.getGson().toJson(inventory))
        );
    }

    public DimChestDto getChest(@Nullable UUID owner, DimCode code, Location location) {
        DimInventoryDto inventory = Objects.isNull(owner)
                ? publicChests.get(code)
                : privateChests.get(owner).get(code);

        return inventory.getChests().stream()
                .filter(chest -> Objects.equals(chest.getLocation(), location))
                .findFirst()
                .orElse(place(owner, code, location));
    }

}
