package me.smourad.dimchest.data;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class DimInventory {

    private Map<String, Object>[] items;
    private List<DimChest> chests;

}
