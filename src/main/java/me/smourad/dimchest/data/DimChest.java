package me.smourad.dimchest.data;

import lombok.Data;

import java.util.Map;
import java.util.UUID;

@Data
public class DimChest {

    private Map<String, Object> location;
    private UUID uuid;

}
