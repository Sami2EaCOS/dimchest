package me.smourad.dimchest.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtils {

    private static final String JSON_FILE = "%s.json";

    private FileUtils() {}

    public static String read(Path dir, String filename) {
        try {
            Files.createDirectories(dir);
            Path path = dir.resolve(String.format(JSON_FILE, filename));
            return read(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void write(Path dir, String filename, String json) {
        try {
            Files.createDirectories(dir);
            Path path = dir.resolve(String.format(JSON_FILE, filename));
            write(path, json.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void delete(Path dir, String filename) {
        Path path = dir.resolve(String.format(JSON_FILE, filename));
        delete(path);
    }

    protected static void delete(Path path) {
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Map<Path, String> readAll(Path dir) {
        try {
            Files.createDirectories(dir);
            try (Stream<Path> files = Files.list(dir)) {
                return files
                        .filter(Files::isRegularFile)
                        .collect(Collectors.toMap(Function.identity(), FileUtils::read));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Path> getAllFolders(Path dir) {
        try {
            Files.createDirectories(dir);
            try (Stream<Path> files = Files.list(dir)) {
                return files
                        .filter(Files::isDirectory)
                        .toList();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected static String read(Path path) {
        try {
            return new String(Files.readAllBytes(path));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean exist(Path dir, String filename) {
        Path path = dir.resolve(String.format(JSON_FILE, filename));
        return exist(path);
    }

    public static boolean exist(Path path) {
        return Files.exists(path);
    }

    protected static void write(Path path, byte[] data) {
        try {
            Files.write(path, data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
