package me.smourad.dimchest.dto;

import lombok.Data;
import org.bukkit.inventory.Inventory;

import java.util.List;

@Data
public class DimInventoryDto {

    private Inventory inventory;
    private List<DimChestDto> chests;

}
