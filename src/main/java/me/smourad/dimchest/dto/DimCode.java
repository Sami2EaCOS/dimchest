package me.smourad.dimchest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.Material;

@Data
@AllArgsConstructor
public class DimCode {

    private Material first;
    private Material second;
    private Material third;

}
