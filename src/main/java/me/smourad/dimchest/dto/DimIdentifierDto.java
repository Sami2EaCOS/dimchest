package me.smourad.dimchest.dto;

import org.bukkit.Material;

import java.util.List;
import java.util.UUID;

public record DimIdentifierDto(DimCode code, UUID owner) {

}
