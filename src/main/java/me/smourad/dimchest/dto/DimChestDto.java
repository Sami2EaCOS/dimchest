package me.smourad.dimchest.dto;


import lombok.Data;
import org.bukkit.Location;

import java.util.UUID;

@Data
public class DimChestDto {

    private Location location;
    private UUID uuid;

}
